# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.exceptions import except_orm, Warning, RedirectWarning
import time, datetime


class ql_chung_cu_toa_nha(models.Model):
    _name = "apartment.building"

    name = fields.Char("Tên")


class ql_chung_cu_phong(models.Model):
    _name = "apartment.room"


    name = fields.Char("Tên tòa nhà")
    building_id = fields.Many2one('apartment.building', 'Tòa nhà')


class ql_chung_cu_hop_dong(models.Model):
    _name = "apartment.contract"

    name = fields.Char('Số hợp đồng', required=True)
    room_id = fields.Many2one('apartment.room', "Phòng", required=True)
    price = fields.Float('Đơn giá')
    deposit = fields.Float('Số tiền đặt cọc')
    date_created = fields.Date('Ngày tạo hợp đồng')
    date_start = fields.Date('Ngày bắt đầu hợp đồng', required=True)
    date_end = fields.Date('Ngày kết thúc hợp đồng')
    water_start = fields.Float('Chỉ số nước ban đầu', required=True)
    power_start = fields.Float('Chỉ số điện ban đầu', required=True)
    supplier_id = fields.Many2one('res.partner', 'Khách hàng', required=True)
    is_active = fields.Boolean('Active')
    state = fields.Selection([
        ('draft', 'Nháp'),
        ('confirm', 'Xác nhận'),
        ('cancel', 'Đã hủy')], 'Trạng thái')
    _defaults = {
        'state': 'draft',
        'date_created': time.strftime('%Y-%m-%d')
    }

    @api.one
    def action_confirm(self):
        pre_month = self.env['apartment.month.index'].search([('room_id', '=', self.room_id.id),
                                                                      ('state', '=', 'confirm')],
                                                                     order='date, id desc', limit=1)
        contract_ids = self.search([('room_id', '=', self.room_id.id), ('state', '=', 'confirm')],
                                   order='date_start desc', limit=1)  # get hop dong truoc do
        if contract_ids:
            if (contract_ids.date_end == False):
                raise except_orm('Lỗi!', 'Vui lòng thêm ngày kết thúc vào hợp đồng số %s' % contract_ids.name)
            elif contract_ids.date_end > self.date_start:
                raise except_orm('Lỗi!',
                                 'Không thể xác nhận 1 hợp đồng có ngày bắt đầu nhỏ hơn ngày kết thúc của số hợp đồng %s' % contract_ids.name)
            elif self.date_end < self.date_start:
                raise except_orm('Lỗi!',
                                 'Ngày kết thúc hợp đồng không thể nhỏ hơn ngày bắt đầu hợp đồng')
            else:
                month = self.env['apartment.month'].search(
                [('year', '=', datetime.datetime.strptime(self.date_start, '%Y-%m-%d').strftime("%Y")),
                 ('date_start', '<=', self.date_start), ('date_end', '>=', self.date_start)])
                if (pre_month.water_number < self.water_start or pre_month.power_number < self.power_start):
                    self.env['apartment.month.index'].create({
                    'room_id': self.room_id.id,
                    'date': self.date_start,
                    'month': month.id,
                    'water_number': self.water_start,
                    'power_number': self.power_start,
                    'state': 'confirm'
                    })
                    self.state = 'confirm'
                else:
                    raise except_orm('Lỗi!',
                                     'Chỉ số mới không thể nhỏ hơn chỉ số cũ!' )

        else:
            month = self.env['apartment.month'].search(
                [('year', '=', datetime.datetime.strptime(self.date_start, '%Y-%m-%d').strftime("%Y")),
                 ('date_start', '<=', self.date_start), ('date_end', '>=', self.date_start)])
            if (pre_month.water_number < self.water_start or pre_month.power_number < self.power_start):
                self.env['apartment.month.index'].create({
                    'room_id': self.room_id.id,
                    'date': self.date_start,
                    'month': month.id,
                    'water_number': self.water_start,
                    'power_number': self.power_start,
                    'state': 'confirm'
                })
                self.state = 'confirm'
            else:
                    raise except_orm('Lỗi!',
                                     'Chỉ số mới không thể nhỏ hơn chỉ số cũ!')

    @api.one
    def action_cancel(self):
        self.state = 'cancel'


class ql_chung_cu_month(models.Model):
    _name = "apartment.month"

    name = fields.Char('Tháng', requieqred=True)
    year = fields.Selection([
        ('2015', '2015'),
        ('2016', '2016'),
        ('2017', '2017'),
        ('2018', '2018'),
        ('2019', '2019'),
        ('2020', '2020')], 'Năm', requieqred=True)
    date_start = fields.Date('Ngày bắt đầu')
    date_end = fields.Date('Ngày kết thúc')
    _defaults = {
        'year': datetime.date.today().strftime('%Y')
    }


class ql_chung_cu_dien_nuoc(models.Model):
    _name = "apartment.month.index"
    _order = 'id desc'

    name = fields.Char(compute='_get_name')
    room_id = fields.Many2one('apartment.room', "Phòng", required=True)
    date = fields.Date('Ngày ghi chỉ số', required=True)
    old_water = fields.Float('Chỉ số nước cũ', states={'confirm': [('readonly', True)]})
    old_power = fields.Float('Chỉ số điện cũ', states={'confirm': [('readonly', True)]})
    water_number = fields.Float("Chỉ số nước mới", required=True)
    power_number = fields.Float("Chỉ số điện mới", required=True)
    month = fields.Many2one("apartment.month", "Tháng", required=True)
    state = fields.Selection([
        ('draft', 'Nháp'),
        ('confirm', 'Xác nhận'),
        ('cancel', 'Đã hủy')], 'Trạng thái')
    _defaults = {
        'state': 'draft',
         'date': time.strftime('%Y-%m-%d')
    }

    @api.model
    def create(self, vals):
        cxt = self.env.context
        action = self.env['ir.actions.act_window'].browse(cxt['params']['action'])
        if action.res_model == 'apartment.month.index':
            if (vals['old_water'] > vals['water_number'] or vals['old_power'] > vals['power_number']):
                raise except_orm('Lỗi!', 'Chỉ số mới không thể nhỏ hơn chỉ số cũ')
            else:
                return super(ql_chung_cu_dien_nuoc, self).create(vals)
        else:
            return super(ql_chung_cu_dien_nuoc, self).create(vals)

    @api.multi
    def _get_name(self):
        for record in self:
            record.name = self.room_id.name + ' - ' + self.month.name

    @api.one
    def action_confirm(self):
        self.state = 'confirm'

    @api.one
    def action_cancel(self):
        self.state = 'cancel'

    @api.onchange('room_id')
    def onchange_service(self):
        if self.room_id:
            pre_month = self.env['apartment.month.index'].search([('room_id', '=', self.room_id.id),
                                                                  ('state', '=', 'confirm'),
                                                                  ('date', '<=', self.date)],
                                                                 order='date desc', limit=1)
            if pre_month:
                self.old_water = pre_month.water_number
                self.old_power = pre_month.power_number
            else:
                raise except_orm('Lỗi!', 'Có lỗi xảy ra')


class apartment_price(models.Model):
    _name = 'apartment.price'
    """
        module cấu hình giá điện nước cơ bản
    """

    water_price = fields.Float('Giá nước/m3')
    power_price = fields.Float('Giá điện/Kwh')


class apartment_service_conf(models.Model):
    _name = 'apartment.service.conf'
    """
        module cấu hình các loại dịch vụ cơ bản
    """
    name = fields.Char('Tên dịch vụ')

class apartment_service(models.Model):
    _name = 'apartment.service'

    """
        Module cho phép người dùng nhập dịch vụ sử dụng cho tòa nhà
    """
    name = fields.Char(compute='_get_name')
    room_id = fields.Many2one('apartment.room', "Phòng", required=True)
    date = fields.Date('Ngày ghi dịch vụ', required=True)
    service_type = fields.Many2one('apartment.service.conf', 'Loại dịch vụ')
    month = fields.Many2one("apartment.month", "Tháng", required=True)
    price = fields.Float('Tổng tiền')
    description = fields.Char('Ghi chú')
    state = fields.Selection([
        ('draft', 'Nháp'),
        ('confirm', 'Xác nhận'),
        ('cancel', 'Đã hủy')], 'Trạng thái')
    _defaults = {
        'state': 'draft',
         'date': time.strftime('%Y-%m-%d')
    }

    @api.multi
    def _get_name(self):
        for record in self:
            record.name = self.room_id.name + ' - ' + self.service_type.name

    @api.one
    def action_confirm(self):
        self.state = 'confirm'

    @api.one
    def action_cancel(self):
        self.state = 'cancel'

class paid_contract(models.Model):
    _name = 'paid.contract'

    name = fields.Char('Tên')
    room_id = fields.Many2one('apartment.room', 'Tên phòng', readonly=True)
    supplier_id = fields.Many2one('res.partner', 'Khách hàng', readonly=True)
    month = fields.Many2one("apartment.month", "Tháng", required=True)
    power_price = fields.Float('Tiền điện', readonly=True)
    water_price = fields.Float('Tiền nước', readonly=True)

    @api.model
    def default_get(self, fields_list):
        defaults = {
        }
        cxt = self.env.context
        if cxt.get('active_model') == 'apartment.contract':
            contract = self.env['apartment.contract'].browse([cxt.get('active_id'),])
            defaults['room_id'] = contract.room_id.id
            defaults['supplier_id'] = contract.supplier_id.id
        return defaults

    @api.onchange('month')
    def _onchange_month(self):
        cxt = self.env.context
        contract_id = self.env['apartment.contract'].browse([cxt.get('active_id'),])
        if self.month:
            if contract_id.date_end != False:
                pw = self.env['apartment.month.index'].search([('month', '=', self.month.id),
                                                           ('room_id', '=', self.room_id.id),
                                                           ('date', '<=', contract_id.date_end)],
                                                            order = 'date desc')
                print pw[0].power_number
                print pw[1].power_number
                price_list = self.env['apartment.price'].search([], order='id desc', limit = 1)
                print price_list.power_price
                self.power_price = (pw[1].power_number - pw[0].power_number)*price_list.power_price
                self.water_price = (pw[1].water_number - pw[0].water_number)*price_list.water_price
            else:
                pw = self.env['apartment.month.index'].search([('month', '=', self.month.id),
                                                           ('room_id', '=', self.room_id.id)],
                                                            order='date desc', limit=2)
                print pw
                print pw[0].power_number

                price_list = self.env['apartment.price'].search([], order='id desc', limit = 1)
                print price_list.power_price
                self.power_price = (pw[0].power_number - pw[1].power_number)*price_list.power_price
                self.water_price = (pw[0].water_number - pw[1].water_number)*price_list.water_price





